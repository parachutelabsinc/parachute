//
//  ZoneCreationViewController.swift
//  Parachute
//
//  Created by Michael Jean on 2015-08-31.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices


class ZoneCreationViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    @IBOutlet weak var zoneDetailLabel: UILabel!
    @IBOutlet weak var creatingZoneLabel: UILabel!
    @IBOutlet weak var zoneNameTextField: UITextField!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var endingDateButton: UIButton!
    @IBOutlet weak var imagePicker: UIImageView!
    @IBOutlet weak var eventEndDateLabel: UILabel!
    @IBOutlet weak var zoneDescriptionTextField: UITextView!
    
    var dateFormatter = NSDateFormatter() // to save the format for a date
    var dateSelected: Bool = false
    var resizedImage = UIImage()
    var firstPoint = CLLocationCoordinate2D() //a single reference coordinate passed from mapview VC
    var mergedPoints = String() //a string of all the coordinates latitude, longitude separated by white space passed from mapview VC
    var userPoints = PFObject(className: "userZones")
    
    let shortTap = UITapGestureRecognizer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let shortTap = UITapGestureRecognizer(target: self, action: "tap")
        self.view.addGestureRecognizer(shortTap)
        
        zoneNameTextField.text = "Zone Name"
        zoneNameTextField.textColor = UIColor.lightGrayColor()
        
        zoneDescriptionTextField.text = "Description"
        zoneDescriptionTextField.textColor = UIColor.lightGrayColor()
        
        self.zoneDescriptionTextField.delegate = self
        self.zoneNameTextField.delegate = self
        
        //hide the date picker
        datePicker.hidden = true
        activitySpinner.hidden = true
        creatingZoneLabel.hidden = true
        
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm "
        self.endingDateButton.setTitle(dateFormatter.stringFromDate(NSDate(timeInterval: 3600, sinceDate: NSDate())) + NSTimeZone.localTimeZone().abbreviation!, forState: UIControlState.Normal)
        
        self.imagePicker.layer.cornerRadius = self.imagePicker.frame.size.width/2
        self.imagePicker.clipsToBounds = true
        
        self.navigationItem.title = "Zone Details" // change the title from explore to zone details
        continueButton.hidden = true
        
        //add a tap gesture to the image viewer so users can tap it and add photos
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped")
        imagePicker.addGestureRecognizer(tapGesture)
        imagePicker.userInteractionEnabled = true
        
        //adds a target so we can keep checking for a particular action. In this case we check if the textfield has changed
        //so we can choose when to show the continue button
        zoneNameTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
      
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillAppear:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillDisappear:", name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueButtonTapped(sender: AnyObject) {
        continueButton.enabled = false
        /*
        activitySpinner.hidden = false
        creatingZoneLabel.hidden = false
        activitySpinner.startAnimating()
        */
        storePointsToParse() //code is in the helper functions section - Preps the coordinates for database
    }

    
    @IBAction func endingDateButtonTapped(sender: AnyObject) {
        self.datePicker.hidden = false
        self.view.addSubview(self.datePicker)
        
        self.datePicker.minimumDate = NSDate()
        //self.datePicker.maximumDate

    }
    
    func tap() {
        view.endEditing(true)
        
        let timeZone = NSTimeZone.localTimeZone().abbreviation!
        
        let oneYear = NSDate(timeInterval:  2678400 , sinceDate: NSDate())
        let strDate = dateFormatter.stringFromDate(self.datePicker.date)
        
        if (self.datePicker.date.timeIntervalSinceNow <= 0){
            self.endingDateButton.setTitle(dateFormatter.stringFromDate(NSDate()) + timeZone, forState: UIControlState.Normal)
        }
        
        if (self.datePicker.date.timeIntervalSinceDate(oneYear) > 0){
            self.endingDateButton.setTitle(dateFormatter.stringFromDate(oneYear) + timeZone, forState: UIControlState.Normal)
        }
        
        else {
            self.endingDateButton.setTitle(strDate + timeZone, forState: UIControlState.Normal)
        }
        
        self.view.removeGestureRecognizer(shortTap)
        self.datePicker.hidden = true
    }
    
    //Pulls up camera when the imageviewer is tapped
    func imageTapped() {
        // println("Tap detected")
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        } else {
            NSLog("No Camera.")
            let alert = UIAlertController(title: "No camera", message: "No camera access", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }


    func storePointsToParse(){
        /*
        self.activitySpinner.hidden = false
        self.activitySpinner.startAnimating()
        
        self.view.addSubview(self.activitySpinner)
        self.view.addSubview(self.creatingZoneLabel)
        */
        
        let data = mergedPoints.dataUsingEncoding(NSUTF8StringEncoding)
        let file = PFFile(name: "coordinates.txt", data: data!)
        
        let point = PFGeoPoint(latitude: self.firstPoint.latitude, longitude: self.firstPoint.longitude)
        
        userPoints["username"] = PFUser.currentUser()?.username
        userPoints["zoneCoordinates"] = file
        userPoints["approved"] = "no"
        userPoints["zoneName"] = self.zoneNameTextField.text
        userPoints["zoneDescription"] = self.zoneDescriptionTextField.text
        
        userPoints["singlePt"] = point
        userPoints["eventEndDate"] = endingDateButton.titleLabel!.text
        
        if (userPoints["icon"] == nil) {
            
            let image = UIImage(named:"add-photo-icon@2x.png")
            let imageData = UIImagePNGRepresentation(image!)
            let imageFile = PFFile(name:"icon.png", data:imageData!)
            self.userPoints["icon"] = imageFile
            
        }
        
        userPoints.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                //println("sent to cloud")
                /*
                self.activitySpinner.hidden = true
                self.activitySpinner.stopAnimating()
                
                self.creatingZoneLabel.hidden = true
                */
                self.continueButton.enabled = true
                
                self.performSegueWithIdentifier("unwindID", sender: self)
               // self.dismissViewControllerAnimated(true, completion: {})
                
            } else {
                print("error to cloud")
            }
        }
        
    }
    
    //function to show the continue button when there is content in the textfields
    func textFieldDidChange(textField: UITextField) {
        if(!zoneNameTextField.text!.isEmpty){
            continueButton.hidden = false
        } else {
            continueButton.hidden = true
        }
    }
    
    //Dismiss keyboard when return button is tapped
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if zoneNameTextField.textColor == UIColor.lightGrayColor() {
            zoneNameTextField.text = ""
            zoneNameTextField.textColor = UIColor.blackColor()
        }
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if zoneNameTextField.text!.isEmpty {
            zoneNameTextField.text = "Zone Name"
            zoneNameTextField.textColor = UIColor.lightGrayColor()
        }
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n"){
            textView.resignFirstResponder()
            return false
        }
    return true
    }

    
    func textViewDidBeginEditing(textView: UITextView) {
        if zoneDescriptionTextField.textColor == UIColor.lightGrayColor() {
            zoneDescriptionTextField.text = ""
            zoneDescriptionTextField.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if zoneDescriptionTextField.text.isEmpty {
            zoneDescriptionTextField.text = "Description"
            zoneDescriptionTextField.textColor = UIColor.lightGrayColor()
        }
    }
    
    //When a user picks a photo taken from the camera
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if(mediaType == kUTTypeImage as String){
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            let resizedImage = imageResize(image, targetSize: CGSizeMake(200, 200))
            self.imagePicker.image = resizedImage
            
            let imageData = UIImagePNGRepresentation(resizedImage)
            let imageFile = PFFile(name:"icon.png", data:imageData!)
            self.userPoints["icon"] = imageFile
            
        } else {
            return
        }
    }
    
    //Resize func for images
    func imageResize(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        var newSize = CGSize()
        //let widthRatio = targetSize.width / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        //removed vertical only photos
        // if (widthRatio > heightRatio) {
        newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
        
        // } else {
        //   displayAlertMessage("Please choose a vertical photo.")
        
        //  return UIImage(named:"add-photo-icon@2x.png")!
        // }
    }

    func displayAlertMessage(userMessage: String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    func keyboardWillAppear(notification: NSNotification){
        // Do something here
        /* self.imageView.frame.size = CGRectMake(self.view.frame.size.width/2, 50, 70, 70)
        self.zoneDescriptionTextField
        self.zoneNameTextField */
        if (self.view.frame.origin.y <= -46.0){
            return
        }
        self.view.frame.origin.y -= 110

    }
    
    func keyboardWillDisappear(notification: NSNotification){
        // Do something here
        self.view.frame.origin.y += 110

    }
    
}
