//
//  CellDetailViewController.swift
//  ding
//
//  Created by Michael Jean on 2015-07-29.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit
import MapKit
import Parse

class CellDetailViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var zoneNameTextLabel: UILabel!

    let reuseId = "resusableID" //reuse id for pin creation
    let regionRadius: CLLocationDistance = 1500
    @IBOutlet weak var zoneDescriptionLabel: UITextView!
    @IBOutlet weak var mapView: MKMapView!
    
    var zoneVar: Zone?
    var polygonCoord = [CLLocationCoordinate2D]()
    var stringVersion = [NSString]()
    var locationCopy = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //imageIconView.image = zoneVar?.area
        zoneNameTextLabel.text = zoneVar?.name
        zoneDescriptionLabel.text = zoneVar?.des
        
        queryGeoPts()
        convertPts()

        let polygon = MKPolygon(coordinates: &polygonCoord, count: polygonCoord.count)
        mapView.addOverlay(polygon)
        self.mapView.delegate = self
        
        locationCopy = CLLocation(latitude: polygonCoord[1].latitude, longitude: polygonCoord[1].longitude)
        
        centerMapOnLocation(locationCopy)
        // Do any additional setup after loading the view.
    }

    func queryGeoPts() {
        let query = PFQuery(className: "userZones")
    
        query.cachePolicy = .NetworkElseCache
        query.whereKey("objectId", equalTo: zoneVar!.objId)
    
        let object = query.findObjects()
        let zoneMatches = object as! [PFObject]
        
        for match in zoneMatches {
            let coordinateData = match["zoneCoordinates"] as! PFFile
            let coordinates = coordinateData.getData()
            stringVersion = NSString(data: coordinates!, encoding: NSUTF8StringEncoding)!.componentsSeparatedByString(" ")
        }

    }

    func convertPts(){
        for i in 0..<stringVersion.count {
            if !(i % 2 == 0) {
                polygonCoord.append(CLLocationCoordinate2DMake(stringVersion[i-1].doubleValue, stringVersion[i].doubleValue))
            }
        }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        //if overlay is MKPolygon {
            let polylineRenderer = MKPolygonRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor(hue: 194.0/360, saturation: 0.76, brightness: 0.81, alpha: 0.80)
            polylineRenderer.lineWidth = 11
            
            return polylineRenderer
        //}
    }

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        //println("called delegate")
        
        //if the annotation is not my custom point annotation then disregard
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        var aView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)//create a reusable ID
        
        //if there isn't a pin available to be reused, then make a pin under the same reusable ID
        if aView == nil {
            aView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        else {
            aView!.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
        aView!.image = UIImage(named:cpa.imageName) //set custom pin image
        
        return aView
    }
 
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
}
