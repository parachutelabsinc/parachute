//
//  ZoneTableViewCell.swift
//  ding
//
//  Created by Michael Jean on 2015-07-27.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit

class ZoneTableViewCell: UITableViewCell {
    
    var zoneIconImage = UIImageView()
    var notificationsTextLabel = UILabel()
    var zoneNameTextLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        
        
        //zoneIconImage.frame = CGRectMake(0, 0, 45, 45)
        notificationsTextLabel = UILabel(frame: CGRectMake(self.bounds.size.width - 15, self.bounds.size.height/2, 10, 10))
        zoneNameTextLabel = UILabel(frame: CGRectMake(66, (self.bounds.height - 53) / 2, self.bounds.size.width - 70, 75))
        zoneIconImage = UIImageView(frame: CGRectMake(10, (self.bounds.size.height-26)/2 , 46, 46))
        
        zoneNameTextLabel.font = UIFont(name: "Arial-BoldMT", size: 13)
        zoneNameTextLabel.adjustsFontSizeToFitWidth = true
        zoneNameTextLabel.minimumScaleFactor = 10.0/12.0
        
        zoneNameTextLabel.textColor = UIColor(red: 146/255.0, green: 148/255.0, blue: 151/255.0, alpha: 1)

        self.zoneIconImage.layer.cornerRadius = self.zoneIconImage.frame.size.width/2
        self.zoneIconImage.clipsToBounds = true
        
        self.contentView.addSubview(zoneIconImage)
        self.contentView.addSubview(notificationsTextLabel)
        self.contentView.addSubview(zoneNameTextLabel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /*

    override func layoutSubviews() {
        superview?.layoutSubviews()
        
        notificationsTextLabel = UILabel(frame: CGRectMake(self.bounds.size.width - 10, 22, 10, 10))
        zoneNameTextLabel = UILabel(frame: CGRectMake(self.bounds.size.width/2, 87.5, 50, 50))
        zoneIconImage = UIImageView(frame: CGRectMake(self.bounds.size.width/2, 67.5, 22.5, 22.5))


    }
    */

}
