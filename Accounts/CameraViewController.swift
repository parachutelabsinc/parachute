//
//  CameraViewController.swift
//  ding
//
//  Created by Michael Jean on 2015-07-26.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//
// To use avcam, you must set a preset capture setting, find and set the input device
// if camerabutton crashes, insert programmic button code back into the beginrecording() func

import Parse
import UIKit
import MobileCoreServices
import AVFoundation


struct zoneSelectionData {
    var zoneIcon: UIImage
    var zoneId: String
    var zoneName: String
}

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let captureSession = AVCaptureSession()
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    var captureDeviceFront : AVCaptureDevice?
    var captureDeviceBack : AVCaptureDevice?
    var previewLayer : AVCaptureVideoPreviewLayer?
    let stillImageOutput = AVCaptureStillImageOutput()
    var imageData: NSData?
    var pathString = NSString()
    
    var videoScaleAndCropFactor = CGFloat()
    var counter: Double = 1.0
    var zoneSelectionDataArray = [zoneSelectionData]()
    var resizedImage = UIImage()
    var labelCount = 0
    var label = UITextField()
    
    /*
    var backButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var cameraButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var continueButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var savePhotoButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var timerButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var flashButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var cameraRotateButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    */
    var finished = Bool() //value to determine whether to close the VC
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var savingLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadCamera()
        
        activitySpinner.alpha = 0.0
        savingLabel.alpha = 0.0
        
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func loadCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.videoQuality = UIImagePickerControllerQualityType.TypeMedium
            imagePicker.delegate = self
            imagePicker.sourceType = .Camera;
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = false
            imagePicker.videoMaximumDuration = 12.0
            
            //turn false to put custom camera buttons
            imagePicker.showsCameraControls = true
            
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
        else {
            self.dismissViewControllerAnimated(true, completion: nil)
            displayAlertMessage("No camera access")
        }
    }
    
    override func viewDidAppear(animated: Bool) {

        // New code uncommented
        if finished == true {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
                self.queryNearbyZones() // put back into viewdidload if cells are empty
            }
        }
        

        
        /*
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        let devices = AVCaptureDevice.devices()
        
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if(device.position == AVCaptureDevicePosition.Back) {
                    captureDevice = device as? AVCaptureDevice
                    captureDeviceBack = device as? AVCaptureDevice
                } else {
                    captureDeviceFront = device as? AVCaptureDevice
                }
            }
        }
        /*
        self.imageView.image = UIImage(named: "flash-icon@2x.png")
        var tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped:")
        self.imageView.addGestureRecognizer(tapGesture)
        self.imageView.userInteractionEnabled = true
  
        self.drawButton("continueButton")
        self.queryNearbyZones()
*/
        
        
        if (!captureSession.running && captureDevice != nil){
            beginRecording()
            
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
            self.displayAlertMessage("No camera access")
        }
        */
    }
    
    //check if user is in a zone
    
    /*
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolygon {
            let polygonView = MKPolygonRenderer(overlay: overlay)
            holder = polygonView
            polygonView.strokeColor = UIColor.redColor()
    
            return polygonView
        }
    
    return nil
    }
    
    */
    // if user presses the cancel button without taking vid
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        finished = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //prep the recorded video before uploading to parse + save to users phone
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if (mediaType == kUTTypeMovie as String) {
            
            let tempImage = info[UIImagePickerControllerMediaURL] as! NSURL
            pathString = tempImage.relativePath!
            
            UISaveVideoAtPathToSavedPhotosAlbum(pathString as String, self, nil, nil)
            self.performSegueWithIdentifier("toZoneSelection", sender: self)
            
        }
    }
    
    /*
    func textToImage(drawText: NSString, inImage: UIImage, atPoint:CGPoint)->UIImage{
        
        // Setup the font specific variables
        var textColor: UIColor = UIColor.whiteColor()
        var textFont: UIFont = UIFont(name: "Helvetica Bold", size: 12)!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(inImage.size)
        
        //Setups up the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
        ]
        
        //Put the image into a rectangle as large as the original image.
        inImage.drawInRect(CGRectMake(0, 0, inImage.size.width, inImage.size.height))
        
        // Creating a point within the space that is as bit as the image.
        var rect: CGRect = CGRectMake(atPoint.x, atPoint.y, inImage.size.width, inImage.size.height)
        
        //Now Draw the text into an image.
        drawText.drawInRect(rect, withAttributes: textFontAttributes)
        
        // Create a new image out of the images we have created
        var newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //And pass it back up to the caller.
        return newImage
        
    }
    
    func beginRecording(){
        //captureSession.stopRunning()
        previewLayer?.removeFromSuperlayer()
        
        configureDevice()
        
        var err : NSError? = nil
        captureSession.addInput(AVCaptureDeviceInput(device: captureDevice, error: &err))
        
        if err != nil {
            println("error: \(err?.localizedDescription)")
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer!.bounds = CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)
        previewLayer?.position = CGPointMake(view.bounds.midX, view.bounds.midY)
        previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        let cameraPreview = UIView(frame: CGRectMake(0.0, 0.0, view.bounds.size.width, view.bounds.size.height))
        cameraPreview.layer.addSublayer(previewLayer)
        view.addSubview(cameraPreview)
        self.view.layer.addSublayer(previewLayer)
        previewLayer?.frame = self.view.layer.frame
        
        self.drawButton("backButton")
        self.drawButton("cameraTakeButton")
        //self.drawButton("flashButton")
        //self.drawButton("cameraRotateButton")
        
        
        stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }

        captureSession.startRunning()
    }
    
    func focusTo(value : Float) {
        if let device = captureDevice {
            if(device.lockForConfiguration(nil)) {
                device.setFocusModeLockedWithLensPosition(value, completionHandler: { (time) -> Void in
                    //
                })
                device.unlockForConfiguration()
            }
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch = self.touchPercent(touches.first as! UITouch)
        focusTo(Float(touch.x))
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch = self.touchPercent(touches.first as! UITouch)
        focusTo(Float(touch.x))
    }
    
    
    func touchPercent(touch : UITouch) -> CGPoint {
        // Get the dimensions of the screen in points
        let screenSize = UIScreen.mainScreen().bounds.size
        
        // Create an empty CGPoint object set to 0, 0
        var touchPer = CGPointZero
        
        // Set the x and y values to be the value of the tapped position, divided by the width/height of the screen
        touchPer.x = touch.locationInView(self.view).x / screenSize.width
        touchPer.y = touch.locationInView(self.view).y / screenSize.height
        
        // Return the populated CGPoint
        return touchPer
    }
    
    func configureDevice() {
        if let device = captureDevice {
            device.lockForConfiguration(nil)
            device.focusMode = .Locked
            device.unlockForConfiguration()
        }
        
    }
    
    */
    /*
    func takePhoto(sender: UIButton!){
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                
                self.queryNearbyZones()
               // self.checkIfInZone()
                
                self.imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                self.cameraButton.hidden = true
                self.flashButton.hidden = true
                self.cameraRotateButton.hidden = true
                
                self.imageView.image = UIImage(data: self.imageData!)
                
                self.view.addSubview(self.imageView)
                self.view.addSubview(self.backButton)
                
               // self.drawButton("saveButton")
                //self.drawButton("continueButton")
            //    self.drawButton("timerButton")
                
                var tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped:")
                self.imageView.addGestureRecognizer(tapGesture)
                self.imageView.userInteractionEnabled = true
                
            }
        }
    }
*/
    /*
    func imageTapped(gestureRecognizer: UIGestureRecognizer) {
        // println("Tap detected")
        if labelCount == 0 {
            labelCount = 1
            //let tapPoint: CGPoint = gestureRecognizer.locationInView(self.imageView)
            label = UITextField(frame: CGRectMake(0, 30, self.view.frame.width, 20))
            label.textAlignment = NSTextAlignment.Center
            label.textColor = UIColor.whiteColor()
            label.backgroundColor = UIColor.blackColor()
            self.view.addSubview(label)
            
        }
    }
    
*/
    func queryNearbyZones(){
        self.zoneSelectionDataArray.removeAll()
        
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            if error == nil {

                print(geoPoint)
            
                let qry = PFQuery(className: "userZones")
                qry.whereKey("singlePt", nearGeoPoint: geoPoint!, withinKilometers: 2.0)
                qry.whereKey("approved", equalTo: "yes")
                qry.limit = 25
                qry.orderByAscending("objectId")
                
                let objects = qry.findObjects()
                let approvedZones = objects as! [PFObject]
                for zone in approvedZones {
                    let icon = zone["icon"] as! PFFile
                    print("entered")
                    let data = icon.getData()
                    // let coordinateData = zone["zoneCoordinates"] as! PFFile
                    //let coordinates = coordinateData.getData()
                    
                    /*
                    stringVersion.removeAll()
                    //get coordinates and store into a NSString array.  Each element is either a latitude or longitude.
                    stringVersion = NSString(data: coordinates!, encoding: NSUTF8StringEncoding)!.componentsSeparatedByString(" ") as! [NSString]
                    */
                    let iconToBeUsed = UIImage(data: data!)
                    let objid = zone.objectId
                    let zName = zone["zoneName"] as! String
                    
                    self.zoneSelectionDataArray.append(zoneSelectionData(zoneIcon: iconToBeUsed!, zoneId: objid!, zoneName: zName))
                    print(zName)
                }
                
            } else {
                print("error: \(error?.localizedDescription)")
            }
        }

    }
/*
    func convertPts(){
        self.polygonCoord.removeAll()
        for i in 0..<stringVersion.count {
            if !(i % 2 == 0) {
                polygonCoord.append(CLLocationCoordinate2DMake(stringVersion[i-1].doubleValue, stringVersion[i].doubleValue))
                println("eneterd convert")
            }
        }
    }
    
    func checkIfInZone() -> Bool {
        let testPoint = MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: self.mostRecentLocation.latitude, longitude: self.mostRecentLocation.longitude))
        var mapCoordinateIsInPolygon = Bool()
        
        for overlay in self.invisibleMap.overlays {
            if (overlay.isKindOfClass(MKPolygon)) {
                let polygon = overlay as! MKPolygon
                let polygonRend:MKPolygonRenderer = invisibleMap.rendererForOverlay(polygon) as! MKPolygonRenderer
                
                //convert MKMapPoint tapMapPoint to point in renderer's context...
                let tapRendererPoint = polygonRend.pointForMapPoint(testPoint)
                
                polygonRend.invalidatePath()
                
                mapCoordinateIsInPolygon = CGPathContainsPoint(polygonRend.path, nil, tapRendererPoint, false)
                invisibleMap.removeOverlay(overlay as! MKPolygon)
            }

        }
        
        return mapCoordinateIsInPolygon
    }
    */
    
    /*
    func drawButton(button: String) {
        switch button {
        case "saveButton":
            let savePhotoButton = UIImage(named: "download-photo-icon@2x.png")
            self.savePhotoButton.frame = CGRectMake(self.view.frame.size.width/2 - 16.25, self.view.frame.size.height - 63.5, 32.5, 32.5)
            self.savePhotoButton.setBackgroundImage(savePhotoButton, forState: .Normal)
            self.savePhotoButton.setTitle("", forState: UIControlState.Normal)
            self.savePhotoButton.addTarget(self, action: "savePhotoButton:", forControlEvents: .TouchUpInside)
        
            self.view.addSubview(self.savePhotoButton)
        
        case "cameraTakeButton":
            let cameraTakeImage = UIImage(named: "take-photo-icon@2x.png")
        
            cameraButton.frame = CGRectMake(self.view.frame.size.width/2 - 37.5, self.view.frame.size.height-100, 75, 75)
            cameraButton.setBackgroundImage(cameraTakeImage, forState: .Normal)
            cameraButton.setTitle("", forState: UIControlState.Normal)
            cameraButton.addTarget(self, action: "takePhoto:", forControlEvents: .TouchUpInside)
            cameraButton.imageView?.contentMode = UIViewContentMode.ScaleToFill
        
            self.view.addSubview(cameraButton)
            
        case "continueButton":
            let continueButton = UIImage(named: "send-icon@2x.png")
            self.continueButton.frame = CGRectMake(self.view.frame.size.width - 63, self.view.frame.size.height - 63.5, 43, 32.5)
            //43x33.5
            self.continueButton.setBackgroundImage(continueButton, forState: .Normal)
            self.continueButton.setTitle("", forState: UIControlState.Normal)
            self.continueButton.addTarget(self, action: "continueButton:", forControlEvents: .TouchUpInside)
            
            self.view.addSubview(self.continueButton)
            
        case "backButton":
            let backButtonImage = UIImage(named: "arrow-left-shadow-icon@2x.png")
            
            backButton.setBackgroundImage(backButtonImage, forState: .Normal)
            backButton.frame = CGRectMake(25, self.view.frame.size.height - 66, 25, 35)
            backButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
            backButton.addTarget(self, action: "goBack:", forControlEvents: .TouchUpInside)
            
            self.view.addSubview(backButton)
            
        case "timerButton":
            let timerButtonImage = UIImage(named: "timer-icon@2x.png")
            
            timerButton.setBackgroundImage(timerButtonImage, forState: .Normal)
            timerButton.frame = CGRectMake(self.view.frame.size.width*0.75 - 40, self.view.frame.size.height - 63.5, 32.5, 32.5)
            timerButton.setTitle(String(format:"%.0f", counter), forState: UIControlState.Normal)
            timerButton.addTarget(self, action: "timerIncrement:", forControlEvents: .TouchUpInside)
            
            self.view.addSubview(timerButton)
            
        case "flashButton":
            let flashButtonImage = UIImage(named: "flash-off-icon@2x.png")
            
            flashButton.setBackgroundImage(flashButtonImage, forState: .Normal)
            flashButton.frame = CGRectMake(20, 20, 20, 31.5)
            flashButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
            flashButton.addTarget(self, action: "flashToggle:", forControlEvents: .TouchUpInside)
            
            self.view.addSubview(flashButton)
            
        case "cameraRotateButton":
            let cameraRotateImage = UIImage(named: "rotate-camera-icon@2x.png")
            
            cameraRotateButton.setBackgroundImage(cameraRotateImage, forState: .Normal)
            cameraRotateButton.frame = CGRectMake(self.view.frame.size.width - 41.5, 20, 20, 31.5)
            cameraRotateButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
            cameraRotateButton.addTarget(self, action: "cameraViewToggle:", forControlEvents: .TouchUpInside)
            
            self.view.addSubview(cameraRotateButton)
            
        default:
            break
        }
    }
    

*/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toZoneSelection") {
            let nav = segue.destinationViewController as! UINavigationController
            let destinationVC = nav.topViewController as! ZoneSelectionViewController
            destinationVC.zoneSelectionDataArray = zoneSelectionDataArray
            //self.resizedImage = imageResize(self.imageView.image!, targetSize: CGSizeMake(320, 568))
            destinationVC.imageToPass = self.resizedImage
            destinationVC.vidToPass = self.pathString
            //destinationVC.timeDisplay = counter
            
        }
    }
    
    /*
    func imageResize(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        var newSize = CGSize()
        let widthRatio = targetSize.width / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func flashToggle(send: UIButton!){
        if ((captureDevice?.hasTorch) != nil) {
            captureDevice?.lockForConfiguration(nil)
            if (captureDevice?.torchActive != nil) {
                captureDevice!.torchMode = AVCaptureTorchMode.Off
                flashButton.setBackgroundImage(UIImage(named: "flash-icon@2x.png"), forState: .Normal)
            } else {
                // sets the torch intensity to 100%
                captureDevice!.setTorchModeOnWithLevel(1.0, error: nil)
                flashButton.setBackgroundImage(UIImage(named: "flash-off-icon@2x.png"), forState: .Normal)
            }
            // unlock your device
            captureDevice!.unlockForConfiguration()
        }
    }
    
    
    
    
    func cameraViewToggle(send: UIButton!) {
        if captureDevice?.position == AVCaptureDevicePosition.Back {
            captureDevice = captureDeviceFront
            configureDevice()
        } else {
            captureDevice = captureDeviceBack
            configureDevice()
        }

    }
    
    func goBack(send: UIButton!){
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func continueButton(send: UIButton!){
        self.imageView.image = textToImage(self.label.text, inImage: self.imageView.image!, atPoint:self.label.frame.origin)
        self.performSegueWithIdentifier("toZoneSelection", sender: self)
    }
    
    func savePhotoButton(send: UIButton!){
        self.savePhotoButton.enabled = false
        self.view.addSubview(self.activitySpinner)
        self.view.addSubview(self.savingLabel)
        self.activitySpinner.hidden = false
        self.savingLabel.hidden = false
        self.activitySpinner.startAnimating()
        
        UIView.animateWithDuration(3.0, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
             UIImageWriteToSavedPhotosAlbum(UIImage(data: self.imageData!), nil, nil, nil)
            
            self.activitySpinner.alpha = 1.0
            self.savingLabel.alpha = 1.0
            
            }) { (completion: Bool) -> Void in
                
                self.activitySpinner.alpha = 0.0
                self.savingLabel.alpha = 0.0
                self.activitySpinner.stopAnimating()
                //self.savingLabel.hidden = true
                //self.activitySpinner.hidden = true
        }
        
        self.savePhotoButton.enabled = true
    }
    
    func timerIncrement(send: UIButton!){
        if counter != 8 {
            ++counter
            self.timerButton.setTitle(String(format:"%.0f", counter), forState: UIControlState.Normal)
        } else {
            counter = 1
            self.timerButton.setTitle(String(format:"%.0f", counter), forState: UIControlState.Normal)
        }
    }
    */
    func displayAlertMessage(userMessage: String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
}
