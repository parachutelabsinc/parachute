//
//  SettingsViewController.swift
//  ding
//
//  Created by Michael Jean on 2015-07-20.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit
import Parse

class SettingsViewController: UIViewController {

    let username = PFUser.currentUser()!.username
    let email = PFUser.currentUser()!.email
    var iosVersion = UIDevice.currentDevice().systemVersion
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Settings"
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func feedbackButtonTapped(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.parachuteapp.co/support/feedback/\(self.username!)/\(self.email!)/\(iosVersion)")!)
    }
    
    @IBAction func logoutButtonTapped(sender: AnyObject) {
        PFUser.logOut()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
