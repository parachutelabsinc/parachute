//
//  MapViewController.swift
//  ding
//
//  Created by Michael Jean on 2015-07-08.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

/* SHOWING USER LOCATION
var showsUserLocation: BOOL
var isUserLocationVisable: BOOL
var userLocation: MKUserLocation

PIN SIZE MUST BE FIXED.
*/

import UIKit
import MapKit
import CoreLocation
import Parse

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}

class MapViewController: UIViewController, MKMapViewDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var mapView: MKMapView! //map object
    @IBOutlet weak var plusSignButton: UIButton! //top right button to start making a zone
    @IBOutlet weak var continueButton: UIButton! //bottom green button that appears during certain conditions
    @IBOutlet weak var findLocationButton: UIButton!
    
    let regionRadius: CLLocationDistance = 1000
    let reuseId = "resusableID" //reuse id for pin creation
    let dropPin = MKPointAnnotation()
    
    var locationManager = CLLocationManager()
    var points: [CLLocationCoordinate2D] = [] // array to hold the coordinates picked by the user
    var pointsAsStrings: [String] = [] // array to hold the points converted from CLLocationCoordinate 2D to string
    var mergedPoints = String() //a string of all the coordinates latitude, longitude separated by white space
    
    var pinLock: Bool = false //bool value to lock the pin when a zone is complete
    var firstPin: Bool = true //bool value to signify when the first pin dropped to disable titled afterwards
    var myLocation = CLLocationCoordinate2D()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Explore"
         myLocationButton(self)
        /* 
            Hide the zone description/name field/details label and continue button, and 
            show them after the user has plotted points
        */
        continueButton.hidden = true

        self.mapView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /********************************************************
    *********************************************************
    ******************** BUTTON FUNCTIONS *******************
    *********************************************************
    ********************************************************/
    
    @IBAction func myLocationButton(sender: AnyObject) {
        //self.mapView.removeAnnotation(dropPin)
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        self.mapView.showsUserLocation = true
        //dropPin.coordinate = CLLocationCoordinate2DMake(43.656154, -79.369636)
        //dropPin.title = "Your location"
        //mapView.addAnnotation(dropPin)
        
    }
    
    
    
    //Dismisses the mapview storyboard when the back button is tapped
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    
    //Allows user to start plotting pins to create a zone
    @IBAction func plusSignTapped(sender: AnyObject) {
        self.navigationItem.title = "Create Zone"
        
        plusSignButton.hidden = true //hide the add zone button
        self.findLocationButton.hidden = true
        
        //creates a gesture recognizer for a long tap of 0.5 seconds
        let longPress = UILongPressGestureRecognizer(target: self, action: "action:")
        longPress.minimumPressDuration = 0.5
        
        //add the above gesture to the mapview
        mapView.addGestureRecognizer(longPress)
    }
    
    
    /*
    Hides the map view, and continue button when the continue button is tapped the first time
    This will reveal the zone details page that is under the map view which are
    the zone description,name and label
    */
    @IBAction func continueButtonTapped(sender: AnyObject) {
        self.performSegueWithIdentifier("toZoneDetails", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toZoneDetails") {
            
            let nav = segue.destinationViewController as! UINavigationController
            let destinationVC = nav.topViewController as! ZoneCreationViewController
            destinationVC.firstPoint = self.points[0]
            destinationVC.mergedPoints = self.mergedPoints
            /*
            let destinationVC = segue.destinationViewController as! ZoneCreationViewController
            destinationVC.firstPoint = self.points[0]
            destinationVC.mergedPoints = self.mergedPoints
*/
        }
    }
    
    
    /********************************************************
    *********************************************************
    ***************** MAP VIEW FUNCTIONS ********************
    *********************************************************
    ********************************************************/
    
    //Provides a square box around a center point
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        self.mapView.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
    }
    
    /*
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemark: [CLPlacemark]?, error: NSError?) -> Void in
            if error != nil {
                print("Can't update location")
                print("error: \(error?.localizedDescription)" )
                return
            } else {
            
                let location = locations.last
                let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
                self.mapView.setRegion(region, animated: true)
                self.locationManager.stopUpdatingLocation()
            
            }
            /*
            if (placemark!.count > 0){
                let pm = placemark!.first
                self.locationManager.stopUpdatingHeading()
                self.myLocation = pm!.location!.coordinate
                self.centerMapOnLocation(CLLocation(latitude: self.myLocation.latitude, longitude: self.myLocation.longitude))
            }*/
        }
    }
    */
    /*
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {
            (placemark: [AnyObject]!, error: NSError!) -> Void in
            if error != nil {
                print("error:" + error.localizedDescription)
                return
            }
            
            if placemark.count > 0 {
                let pm = placemark[0] as! CLPlacemark
                self.locationManager.stopUpdatingLocation()
                self.myLocation = pm.location.coordinate
                self.centerMapOnLocation(CLLocation(latitude: self.myLocation.latitude, longitude: self.myLocation.longitude))
                
            }
            
        })
    }
    */
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("error:" + error.localizedDescription)
        print("test4")
    }
    
    //code to deal with the pin drop gesture
    func action(gestureRecognizer:UIGestureRecognizer) {
        let touchPoint = gestureRecognizer.locationInView(self.mapView)
        let newCoord:CLLocationCoordinate2D = mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        let newAnotation = CustomPointAnnotation()
        
        if gestureRecognizer.state != .Began { return } //remove the problem with tap and drag pins dropping
        if pinLock == true { return } //once the zone pins are finished, disable pin drops gesture
        
        newAnotation.coordinate = newCoord //set the new pin objects coordinate to the coordinate picked by user tap
        points.append(newAnotation.coordinate) //append that coordinate to the points array to keep history
        
        //if the pin is the first one dropped, then provide a title so it is clickable else remove the title
        if (firstPin == true){
            newAnotation.title = "Initial Pin"
            newAnotation.subtitle = ":)"
            newAnotation.imageName = "pinImage.png"
            firstPin = false
        } else {
            newAnotation.title = ""
            newAnotation.subtitle = ""
            newAnotation.imageName = "pinImage.png"
        }

        //add the new pin to the mapview
        mapView.addAnnotation(newAnotation)
        
        //provide MKPolyline with the coordinates to draw custom lines for and add them to map overlay
        let overlayPin = MKPolyline(coordinates: &points, count: points.count)
        self.mapView.addOverlay(overlayPin)
    }

    
    //when there is a new object to render on the mapview, check what kind of overlay it and
    //if it is a mkpolyline then draw a custom line
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
      //  if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor(hue: 194.0/360, saturation: 0.76, brightness: 0.81, alpha: 0.80)
            polylineRenderer.lineWidth = 11
            
            return polylineRenderer
       // }
        
        
    }

    
    /*
    if an anotation is selected on the screen, in this case it is only previous dropped pins, then
    check if there are 3 or more pins already dropped and check if the first pin was selected
    otherwise return
    */
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
       // println("pin tapped")
        if (points.count < 3) { return }
        
        /*
        create a temp array to hold first and last pin coordinates so we can check if the 
        first pin was pressed to allow the last line to be drawn from the last coordinate.
        Store the first and last coordinate into this temp array
        */
        var firstLastPin: [CLLocationCoordinate2D] = []
        firstLastPin.append(points.first!)
        firstLastPin.append(points.last!)
        
        //check to see if there are 3 or more pins on the map and if the first pin was tapped
        if ((points.count >= 3) && (view.annotation!.coordinate.latitude == firstLastPin.first?.latitude) &&
            (view.annotation!.coordinate.longitude == firstLastPin.first?.longitude)){
            
            //draw a line from the last coordinate to the first coordinate
            let overlayPin = MKPolyline(coordinates: &firstLastPin, count: 2)
            self.mapView.addOverlay(overlayPin)
            
            pinLock = true // lock the pins so no more can be dropped
            printPointsArray() //convert the points to an array of strings and then format it to a single long string
                
            continueButton.hidden = false //reveal the contine button since the zone pins are placed
            self.view.addSubview(self.continueButton)
        } else {
            return
        }
        
    }
    
    
    //create a view for annotations placed on the map with a custom image
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        //println("called delegate")
        
        //if the annotation is not my custom point annotation then disregard
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        var aView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)//create a reusable ID
        
        //if there isn't a pin available to be reused, then make a pin under the same reusable ID
        if aView == nil {
            aView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        else {
            aView!.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
        aView!.image = UIImage(named:cpa.imageName) //set custom pin image
        
        return aView
    }
    
    /********************************************************
    *********************************************************
    ******************** HELPER FUNCTIONS *******************
    *********************************************************
    ********************************************************/
    
    func printPointsArray(){
        for point in points{
            //println("Latitude: \(point.latitude)  Longitude: \(point.longitude)")
            pointsAsStrings.append("\(point.latitude)")
            pointsAsStrings.append("\(point.longitude)")
        }
        
        joinArrayToString(pointsAsStrings)
    }
    
    func joinArrayToString(arrayToJoin: [String]){
        mergedPoints = arrayToJoin.joinWithSeparator(" ")
    }
    
}
