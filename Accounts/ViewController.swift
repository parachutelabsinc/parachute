// View controller for the protected page (main screen)
//  ViewController.swift
//  Accounts
//
//  Created by Michael Jean on 2015-06-12.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import Parse
import UIKit
import MobileCoreServices

struct Zone {
    var name: String
    var photo: UIImage!
    var notification = "0"
    var objId: String
    var des: String
}

class ViewController: UIViewController, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, UIScrollViewDelegate{
    @IBOutlet weak var createAZoneButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var zoneNameTextLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var trendingLabel: UILabel!
    @IBOutlet weak var nearbyLabel: UILabel!
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    var tableView: UITableView = UITableView()
    var tableView2: UITableView = UITableView()
    var zonesArray = [Zone]()
    var zonesArray2 = [Zone]()
    var refreshControl = UIRefreshControl()
    var refreshControl2 = UIRefreshControl()
    var longPress = UILongPressGestureRecognizer()
    var longPress2 = UILongPressGestureRecognizer()
    
    var selectedCell: Zone?
    var zoneCellCopy: Zone?
    let cellIdentifier = "ZoneTableViewCell"
    var objIdCopy = String()
    var mostRecentLocation = PFGeoPoint()
    
    //start to cache the images + data,
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.delegate = self
        
        //self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        //create prototype cell
        self.scrollView.contentSize.width = self.view.frame.width * 2
        
        //add an additional line at the top of the uitableview
        let px = 1 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, 0, self.tableView.frame.size.width, px)
        let line: UIView = UIView(frame: frame)
        self.tableView.tableHeaderView = line
        line.backgroundColor = self.tableView.separatorColor
        
        //register the table's cells using another zoneTableViewCell VC and set the cell height
        tableView.registerClass(ZoneTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView2.registerClass(ZoneTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        self.tableView.rowHeight = 63
        self.tableView2.rowHeight = 63
        
        self.tableView.frame = CGRectMake(0, 0, self.view.frame.width, view.frame.height-180)
        self.tableView.separatorInset = UIEdgeInsetsZero
        self.scrollView.addSubview(self.tableView)
        
        self.tableView2.separatorInset = UIEdgeInsetsZero
        self.tableView2.frame = CGRectMake(view.frame.width, 0, self.view.frame.width, view.frame.height - 180)
        self.scrollView.addSubview(tableView2)
        
        //asynchronously populate table cells
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            self.setTableCells()
            
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView2.delegate = self
            self.tableView2.dataSource = self
        }
        
        let logo = UIImage(named: "parachute-logo-beta@2x.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        refreshControl.addTarget(self, action: "setTableCells", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl2.addTarget(self, action: "setTableCells", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        self.tableView2.addSubview(refreshControl2)

        
        longPress = UILongPressGestureRecognizer(target: self, action: "action:")
        longPress2 = UILongPressGestureRecognizer(target: self, action: "action:")
        longPress.minimumPressDuration = 0.2
        longPress2.minimumPressDuration = 0.2
        tableView.addGestureRecognizer(longPress)
        tableView2.addGestureRecognizer(longPress2)
        
        
    }

    //control the bolding of page dots and trending/nearby labels
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if (view.frame.origin.x < scrollView.contentOffset.x) {
            ++pageIndicator.currentPage
            print("To second screen")
            print(view.frame.origin.x, scrollView.contentOffset.x)
            trendingLabel.textColor = UIColor(red: 164/255.0, green: 218/255.0, blue: 241/255.0, alpha: 1)
            nearbyLabel.textColor = UIColor(red: 40/255.0, green: 168/255.0, blue: 224/255.0, alpha: 1)
        }
//self.view.frame.origin.x
        if (self.view.frame.origin.x >= scrollView.contentOffset.x){
            --pageIndicator.currentPage
            print("To first screen")
            print(self.view.frame.width, scrollView.contentOffset.x, view.frame.origin.y, scrollView.contentOffset.y)
            trendingLabel.textColor = UIColor(red: 40/255.0, green: 168/255.0, blue: 224/255.0, alpha: 1)
            nearbyLabel.textColor = UIColor(red: 164/255.0, green: 218/255.0, blue: 241/255.0, alpha: 1)
            
        }
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(tableView == self.tableView){
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ZoneTableViewCell
            let zoneSelection = zonesArray[indexPath.row]
            
            cell.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
            cell.preservesSuperviewLayoutMargins = false
        
            cell.zoneIconImage.image = zoneSelection.photo
            cell.zoneNameTextLabel.text = zoneSelection.name
            cell.notificationsTextLabel.text = zoneSelection.notification

            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ZoneTableViewCell
            let zoneSelection = zonesArray2[indexPath.row]
            
            cell.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
            cell.preservesSuperviewLayoutMargins = false
            
            cell.zoneIconImage.image = zoneSelection.photo
            cell.zoneNameTextLabel.text = zoneSelection.name
            cell.notificationsTextLabel.text = zoneSelection.notification
            
            return cell
        }
    }

    func setTableCells2(){
        self.tableView2.allowsSelection = false
        self.tableView2.scrollEnabled = false
        
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            if error == nil {
                self.mostRecentLocation = geoPoint!
                
                self.zonesArray2.removeAll()
                
                let query = PFQuery(className: "userZones")
                
                query.cachePolicy = .NetworkElseCache
                query.whereKey("approved", equalTo: "yes")
                query.limit = 50
                query.orderByAscending("objectId")
                query.whereKey("singlePt", nearGeoPoint: self.mostRecentLocation, withinKilometers: 75.0)
                
                query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]?, error: NSError?) -> Void in
                    if (error == nil) {
                        let approvedZones = objects as! [PFObject]
                        
                        for item: PFObject in approvedZones as [PFObject] {
                            let image = item["icon"] as! PFFile
                            
                            image.getDataInBackgroundWithBlock({ (icon: NSData?, error: NSError?) -> Void in
                                if (error == nil){
                                    let iconToBeUsed = UIImage(data: icon!)
                                    self.zonesArray2.append(Zone(
                                        name: item["zoneName"] as! String,
                                        photo: iconToBeUsed,
                                        notification: "",
                                        objId: item.objectId!,
                                        des: item["zoneDescription"] as! String))
                                    
                                    self.tableView2.reloadData()
                                }
                            })
                        }
                    } else {
                        print("error: \(error?.localizedDescription)")
                        self.displayAlertMessage((error?.localizedDescription)!)
                    }
                    
                    self.refreshControl2.endRefreshing()
                    self.scrollView.scrollEnabled = true
                    self.tableView2.scrollEnabled = true
                    self.tableView2.allowsSelection = true
                }
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewDidAppear(animated: Bool) {
        // when protected page is created and loaded,
        // override to toLoginPage segway if user isn't logged on
        let currentUser = PFUser.currentUser()
        if (currentUser == nil){
            self.performSegueWithIdentifier("toLoginPage", sender: self)
        } else {
        
        }
        
    }
    

    
    //set up so refresh is locked while refreshing
    func setTableCells(){
        self.tableView.allowsSelection = false
        self.tableView.scrollEnabled = false
        
        self.scrollView.scrollEnabled = false
        
        self.setTableCells2()
        
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            if error == nil {
               self.mostRecentLocation = geoPoint!
                
                self.zonesArray.removeAll()
                
                let query = PFQuery(className: "userZones")
                
                query.cachePolicy = .NetworkElseCache
                query.whereKey("approved", equalTo: "yes")
                query.limit = 50
                query.orderByAscending("objectId")
                //query.whereKey("singlePt", nearGeoPoint: self.mostRecentLocation, withinKilometers: 75.0)
                
                query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]?, error: NSError?) -> Void in
                    if (error == nil) {
                        let approvedZones = objects as! [PFObject]
                        
                        for item: PFObject in approvedZones as [PFObject] {
                            let image = item["icon"] as! PFFile

                            image.getDataInBackgroundWithBlock({ (icon: NSData?, error: NSError?) -> Void in
                                if (error == nil){
                                    let iconToBeUsed = UIImage(data: icon!)
                                    self.zonesArray.append(Zone(
                                        name: item["zoneName"] as! String,
                                        photo: iconToBeUsed,
                                        notification: "",
                                        objId: item.objectId!,
                                        des: item["zoneDescription"] as! String))
                                    
                                    self.tableView.reloadData()
                                }
                            })
                        }
                    } else {
                        print("error: \(error?.localizedDescription)")
                        self.displayAlertMessage((error?.localizedDescription)!)
                    }
                    
                    self.refreshControl.endRefreshing()
                    
                    self.tableView.scrollEnabled = true
                    self.tableView.allowsSelection = true
                }
            }
        }
    }

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView{
            return zonesArray.count
        } else {
            return zonesArray2.count
        }
    }

    
    //code for press and hold popup information
    func action(gestureRecognizer:UIGestureRecognizer) {
        if gestureRecognizer.state != .Began { return } //remove the problem with tap and drag pins dropping
        
        if gestureRecognizer == longPress {
            let location: CGPoint = gestureRecognizer.locationInView(tableView)
            let indexPath = tableView.indexPathForRowAtPoint(location)
            if (indexPath != nil){
                selectedCell = zonesArray[indexPath!.row]
            }
        } else {
            let location: CGPoint = gestureRecognizer.locationInView(tableView2)
            let indexPath = tableView2.indexPathForRowAtPoint(location)
            if (indexPath != nil){
                selectedCell = zonesArray2[indexPath!.row]
            }
        }
        
        // pass data through zoneCellCopy using prepareforsegue
        zoneCellCopy = Zone(name: selectedCell!.name, photo: selectedCell!.photo, notification: selectedCell!.notification, objId: selectedCell!.objId, des: selectedCell!.des)
        
        
        let newVC = self.storyboard!.instantiateViewControllerWithIdentifier("toCellViewDetails") as! CellDetailViewController
        
        newVC.modalPresentationStyle = .Popover
        newVC.preferredContentSize = CGSizeMake(270, 398)

        
        newVC.zoneVar = zoneCellCopy
        
        let overLay = newVC.popoverPresentationController
        overLay?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        overLay?.delegate = self
        overLay?.sourceView = self.view //sender
        let x = UIScreen.mainScreen().bounds.size.width
        let y = UIScreen.mainScreen().bounds.size.height
        
        overLay?.sourceRect = CGRectMake(x/2, y*0.4, 1, 1)
        
        self.presentViewController(newVC, animated: true, completion: nil)

    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if (tableView == self.tableView){
            let zoneSelected = zonesArray[indexPath.row]
            let query = PFQuery(className: "zoneVideos")
            self.objIdCopy = zoneSelected.objId
            query.whereKey("zoneObjectID", equalTo: self.objIdCopy)
        
            query.countObjectsInBackgroundWithBlock { (count: Int32, error: NSError?) -> Void in
            if count == 0 {
                self.displayAlertMessage("No videos in this zone.")
            } else {
                self.performSegueWithIdentifier("toMomentView", sender: self)
            }
            }
        } else {
            let zoneSelected = zonesArray2[indexPath.row]
            let query = PFQuery(className: "zoneVideos")
            self.objIdCopy = zoneSelected.objId
            query.whereKey("zoneObjectID", equalTo: self.objIdCopy)
            
            query.countObjectsInBackgroundWithBlock { (count: Int32, error: NSError?) -> Void in
                if count == 0 {
                    self.displayAlertMessage("No videos in this zone.")
                } else {
                    self.performSegueWithIdentifier("toMomentView", sender: self)
                }
            }
        }
    }
  
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toMomentView") {
            let destinationVC = segue.destinationViewController as! MomentViewController
            destinationVC.passedObjectId = objIdCopy
            print(objIdCopy)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController) -> UIModalPresentationStyle {
            return .None
    }

    func displayAlertMessage(userMessage: String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    /********************************************************
    *********************************************************
    ******************** BUTTON FUNCTIONS *******************
    *********************************************************
    ********************************************************/

    
    @IBAction func cameraButtonTapped(sender: AnyObject) {
    
    }

    
    //when the zone button is tapped
    @IBAction func createAZoneButtonTapped(sender: AnyObject) {
        
        //self.performSegueWithIdentifier("protectedToMapView", sender: self)
    }
    
    
    @IBAction func settingsButtonTapped(sender: AnyObject) {
        //self.performSegueWithIdentifier("protectedToSettings", sender: self)
    }

    @IBAction func unwindToVC(segue: UIStoryboardSegue) {
        if ((segue.sourceViewController as? ZoneCreationViewController) != nil) {
            print("Coming from creating a zone")
        }
    }
    

}
