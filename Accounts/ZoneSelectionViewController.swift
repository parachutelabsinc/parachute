//
//  ZoneSelectionViewController.swift
//  ding
//
//  Created by Michael Jean on 2015-08-03.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit
import Parse


class ZoneSelectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
  
    struct zoneSelectedData {
        var zoneIcon: UIImage
        var zoneId: String
        var zoneName: String
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var submittingLabel: UILabel!
    
    
    var zoneSelectionDataArray = [zoneSelectionData]()
    var zoneToPass: zoneSelectedData?
    var imageToPass = UIImage()
    var vidToPass = NSString()
    //var timeDisplay = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Select a Zone"
        
        activitySpinner.hidden = true
        submittingLabel.hidden = true
        
        collectionView.dataSource = self
        collectionView.delegate = self
        continueButton.hidden = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        
    }
      

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       /* if (segue.identifier == "backToCameraView") {
            var destinationVC = segue.destinationViewController as! CameraViewController
        } */
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.continueButton.hidden = false
        self.view.addSubview(continueButton)
        
        let zoneSelected = zoneSelectionDataArray[indexPath.row]
        
        zoneToPass = zoneSelectedData(zoneIcon: zoneSelected.zoneIcon, zoneId: zoneSelected.zoneId, zoneName: zoneSelected.zoneName)
        print(zoneToPass?.zoneName)
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        cell.layer.backgroundColor = UIColor.lightGrayColor().CGColor
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        cell.layer.backgroundColor = UIColor.whiteColor().CGColor
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        //for now set change the bool value in camera view to get back to home screen while seguing
     //   self.performSegueWithIdentifier("backToCameraView", sender: self)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
 
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return zoneSelectionDataArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: ZoneSelectionCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("zoneCollectionCell", forIndexPath: indexPath) as! ZoneSelectionCollectionViewCell
        let object = zoneSelectionDataArray[indexPath.row]
        cell.zoneLabel.text = object.zoneName
        
        cell.imageView.image = object.zoneIcon
        cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2
        cell.imageView.clipsToBounds = true
        
        return cell
    }
    
    
    @IBAction func continueButtonTapped(sender: AnyObject) {
        activitySpinner.startAnimating()
        activitySpinner.hidden = false
        submittingLabel.hidden = false
        
        self.view.addSubview(activitySpinner)
        self.view.addSubview(submittingLabel)
       
        self.continueButton.enabled = false
        
        let vidFile = PFFile(name: "video.mp4", contentsAtPath: vidToPass as String)
        //let imageData = UIImagePNGRepresentation(imageToPass)
        //let imageFile = PFFile(name: "image.png", data: imageData)
        
        let zoneVideo = PFObject(className:"zoneVideos")
        zoneVideo["zoneObjectID"] = zoneToPass?.zoneId
        zoneVideo["zoneName"] = zoneToPass?.zoneName
        zoneVideo["username"] = PFUser.currentUser()?.username
        zoneVideo["userVid"] = vidFile
        //zonePicture["photos"] = imageFile
       // zonePicture["timeCounter"] = timeDisplay
        
        zoneVideo.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                self.activitySpinner.hidden = true
                self.submittingLabel.hidden = true
                self.activitySpinner.stopAnimating()
                self.performSegueWithIdentifier("toMainScreen", sender: self)
                self.continueButton.enabled = true
                
                
            } else {
                self.activitySpinner.hidden = true
                self.submittingLabel.hidden = true
                self.activitySpinner.stopAnimating()
                self.displayAlertMessage("Error: /(error)")
                self.continueButton.enabled = true
                
                // There was a problem, check error.description
            }
        }
    }

    func displayAlertMessage(userMessage: String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
}
