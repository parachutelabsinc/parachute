//
//  MomentViewController.swift
//  ding
//
//  Created by Michael Jean on 2015-08-02.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit
import Parse
import MediaPlayer
import AVFoundation
import AVKit

class MomentViewController: UIViewController {
    @IBOutlet weak var imageCounter: UILabel!

    var passedObjectId = String()
    var index = 0
    var skipAmount = Int()
    var player = AVPlayer()
    var urlArray = [NSURL]()
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNewVideos()
        
        //add swipedown feature to exit videos at any time
        let swipeDown = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        
        player = AVPlayer(URL: urlArray[index]) //initialize avplayer using video urls in the urlarray
        let playerLayer = AVPlayerLayer(player: player) //add player to player layer
        playerLayer.frame = self.view.frame //create set playerlayer frame size to phones screen size
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill //change aspect ratio to fill entire screen
        self.view.layer.addSublayer(playerLayer) //add the playerlayer to current layer
        player.play() //play the videos in the urlArray
        
        //notify us when a video had finished playing and go to playeritemdidreachend method
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "playerItemDidReachEnd:",
            name: AVPlayerItemDidPlayToEndTimeNotification,
            object: self.player.currentItem)

        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "vidTapped")
        self.view.addGestureRecognizer(tapGesture)
        self.view.userInteractionEnabled = true
    }
    
    func getNewVideos() {
        let query = PFQuery(className: "zoneVideos")
        
        query.cachePolicy = .NetworkElseCache
        query.whereKey("zoneObjectID", equalTo: self.passedObjectId)
        query.limit = 3
        query.skip = skipAmount
        query.orderByAscending("createdAt")
        
        let matches = query.findObjects()
        let videos = matches as! [PFObject]
        
        for vid in videos {
            let video = vid["userVid"] as! PFFile
            ++skipAmount
            self.urlArray.append(NSURL(string: video.url!)!)
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //automatically plays/fetches next videos when video has ended - for when users let videos autoplay without pressing
    func playerItemDidReachEnd(notification: NSNotification) {
        self.vidTapped()
    }
    
    func vidTapped() {
        if (index < urlArray.count - 1){ // -1 so you're able to click on the last vid to exit.  Chec
            ++index
            
            let item = AVPlayerItem(URL: urlArray[index])
            
            player.pause()
            player.replaceCurrentItemWithPlayerItem(item)
            //player = AVPlayer(URL: urlArray[index])
            
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.view.bounds
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.view.layer.addSublayer(playerLayer)
            
            player.play()

            //notify us when a video had finished playing and go to playeritemdidreachend method
            NSNotificationCenter.defaultCenter().addObserver(self,
                selector: "playerItemDidReachEnd:",
                name: AVPlayerItemDidPlayToEndTimeNotification,
                object: self.player.currentItem)

            //if we are 1 video away from finishing the tri-fetched videos, then fetch 3 more videos.
            if (index == urlArray.count - 1) {
                dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
                    self.getNewVideos()
                }
            }
            
            
        } else { //if we do not have any more videos then pause the last video so it doesn't play in the background and dismiss the VC
            player.pause()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
}

    //adds swipe down gesture to exit back to the main screen while viewing videos
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if swipeGesture.direction == UISwipeGestureRecognizerDirection.Down {
                self.dismissViewControllerAnimated(true, completion: nil)
                self.player.pause()
            }
        }
    }
    
}
