//
//  LoginViewController.swift
//  Accounts
//
//  Created by Michael Jean on 2015-06-12.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//
import Parse
import UIKit
import QuartzCore

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var usernameLoginTextField: UITextField!
    @IBOutlet weak var passwordLoginTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    @IBOutlet weak var loginLabel: UILabel!
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activitySpinner.hidden = true
        loginLabel.hidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "DismissKeyboard")
        view.addGestureRecognizer(tap)
        
        signupButton.layer.borderWidth = 2
        signupButton.layer.borderColor = UIColor.whiteColor().CGColor
        loginButton.layer.borderWidth = 2
        loginButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.usernameLoginTextField.delegate = self
        self.passwordLoginTextField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        let currentUser = PFUser.currentUser()
        if (currentUser != nil){
            self.dismissViewControllerAnimated(true, completion: nil)
        }

    }
    
    /********************************************************
    *********************************************************
    ******************** BUTTON FUNCTIONS *******************
    *********************************************************
    ********************************************************/
    
    @IBAction func loginButtonLoginTapped(sender: AnyObject) {
        loginButton.enabled = false
        activitySpinner.hidden = false
        loginLabel.hidden = false
        
        activitySpinner.startAnimating()
        
        self.view.addSubview(activitySpinner)
        self.view.addSubview(loginLabel)
        
        let uname = usernameLoginTextField.text
        let pword = passwordLoginTextField.text
        
        if(uname != "" && pword != ""){
            // user put in a username and password
            signUserIn(uname!.lowercaseString, password: pword!)
            activitySpinner.stopAnimating()
            
            activitySpinner.hidden = true
            loginLabel.hidden = true
            loginButton.enabled = true
        } else {
            // user didn't enter in both a username and password
            activitySpinner.stopAnimating()
            activitySpinner.hidden = true
            loginLabel.hidden = true
            
            loginButton.enabled = true
            displayAlertMessage("Please enter both a valid username and password")
            
        }
        
    }
    
    /********************************************************
    *********************************************************
    ******************** HELPER FUNCTIONS *******************
    *********************************************************
    ********************************************************/

    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    //creates an alert based on the error and provides an OK button on the alert
    
    func displayAlertMessage(userMessage: String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    //helper function to log users in
    func signUserIn(username: String, password: String){
        PFUser.logInWithUsernameInBackground(username, password:password) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                // Do stuff after successful login.
                self.dismissViewControllerAnimated(true, completion:nil)
                
            } else {
                self.displayAlertMessage("Invalid username or password")
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
}
