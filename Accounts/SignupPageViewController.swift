//
//  SignupPageViewController.swift
//  Accounts
//
//  Created by Michael Jean on 2015-06-12.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//
import Parse
import UIKit
import QuartzCore

class SignupPageViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var usernameSignupTextField: UITextField!
    @IBOutlet weak var passwordSignupTextField: UITextField!
    @IBOutlet weak var emailSignupTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var signUpLabel: UILabel!
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activitySpinner.hidden = true
        self.signUpLabel.hidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "DismissKeyboard")
        view.addGestureRecognizer(tap)
        
        registerButton.layer.borderWidth = 2
        registerButton.layer.borderColor = UIColor.whiteColor().CGColor

        self.usernameSignupTextField.delegate = self
        self.passwordSignupTextField.delegate = self
        self.emailSignupTextField.delegate = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /********************************************************
    *********************************************************
    ******************** BUTTON FUNCTIONS *******************
    *********************************************************
    ********************************************************/
    
    @IBAction func existingAccountButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    @IBAction func registerSignupButtonTapped(sender: AnyObject) {
        registerButton.enabled = false
        
        //check if the fields are all filled
        let uname = usernameSignupTextField.text
        let pword = passwordSignupTextField.text
        let email = emailSignupTextField.text
        
        if(uname != "" && uname!.characters.count >= 3 && uname!.characters.count <= 16 && pword!.characters.count >= 8 && pword!.characters.count <= 32 && pword != "" && email != "" && email!.rangeOfString("@") != nil){
            // all required fields are filled, so sign up user and alert them with success and dismiss view
            
            activitySpinner.hidden = false
            signUpLabel.hidden = false
            activitySpinner.startAnimating()
            
            self.view.addSubview(activitySpinner)
            self.view.addSubview(signUpLabel)
            
            signUpUser(uname!.lowercaseString, password: pword!, email: email!.lowercaseString)
            
        } else {
            // required fields are not all filled out and display alert
            registerButton.enabled = true
            displayAlertMessage("Username must be between 3-16 characters and password must be characters 8-32 digits.")
            
        }
        
        
    }

    /********************************************************
    *********************************************************
    ******************** HELPER FUNCTIONS *******************
    *********************************************************
    ********************************************************/
    
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func signUpUser(username: String, password: String, email: String){
        let user = PFUser()
        
        user.username = username
        user.password = password
        user.email = email

        /*
        // other fields can be set just like with PFObject
        user["phone"] = "415-392-0202"
        */
        
        user.signUpInBackgroundWithBlock { (sucess: Bool, error: NSError?) -> Void in
            if error == nil {
                self.registerButton.enabled = true
                self.activitySpinner.stopAnimating()
                self.activitySpinner.hidden = true
                self.signUpLabel.hidden = true
                
                let myAlert = UIAlertController(title: "Alert", message: "Registration successful",
                    preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){ action in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                
                myAlert.addAction(okAction)
                self.presentViewController(myAlert, animated: true, completion: nil)
                
            } else {
                self.registerButton.enabled = true
                
                self.activitySpinner.stopAnimating()
                self.activitySpinner.hidden = true
                self.signUpLabel.hidden = true
                
                self.displayAlertMessage("Could not sign up. \(error)")
            }
        }
    
    }
    
        /*
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if let error = error {
                let errorString = error.userInfo?["error"] as? NSString
                // Show the errorString somewhere and let the user try again.
            } else {
                // Hooray! Let them use the app now and store the object ID
            //    var ojId = user.objectId
            }
        }
        */
    
    //creates an alert based on the error and provides an OK button on the alert
    func displayAlertMessage(userMessage: String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
}
