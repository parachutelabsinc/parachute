//
//  ZoneSelectionCollectionViewCell.swift
//  ding
//
//  Created by Michael Jean on 2015-08-03.
//  Copyright (c) 2015 Ding Labs. All rights reserved.
//

import UIKit

class ZoneSelectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var zoneLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!

}
